### Usage

Having downloaded this repository, copy the `bibtex` folder into your project's folder for external libraries, e.g. `ext` in the same folder as your source folder `src`. Then import this library, e.g. from `src/app.js`, as follows.

```js
import bibtex from '../ext/bibtex/index.js'
```

This library is known to be used by the following projects.

- [https://bitbucket.org/kappe_c/beautify-bibtex/](https://bitbucket.org/kappe_c/beautify-bibtex/)


### Test

To run the tests locally (under Linux), run the following (requires Python >= 3.7).

```sh
python -m http.server 19120 -d ~/dev/bibtex-js/ -b 127.0.0.1
```

Open the output link in a web browser.

Notice that the tests are incomplete.

import unicode from '../bibtex/unicode.js'

import common from './common.js'


const equal_pair = common.equal_pair


const entries = [{title: '\"Apfel und Birnen'}]

const extries = [{title: 'Äpfel und Birnen'}]


const main = () =>
{
	const thisElem = common.make_test_elem('unicode')
	
	unicode.translate(entries)
	
	const numEntries = entries.length
	const fieldsWithSpecialChars = unicode.fieldsWithSpecialChars
	for(let i = 0; i < numEntries; ++i)
		for(const field of fieldsWithSpecialChars)
			if(equal_pair(field, entries[i], extries[i]) === false)
				return 1
	
	return common.pass(thisElem)
}


main()
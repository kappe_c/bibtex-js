import util from '../bibtex/util.js'

import common from './common.js'



const entries = [
	Object.defineProperties({
		Title: 'Foo',
		author: 'Doe, Jane',
		year: 2021,
		month: 'March'
	}, {
		entrytype: { value: 'Article', writable: true },
		id: { value: 'doe2021foo', writable: true}
	}),
	Object.defineProperties({
		title: 'Bar',
		AUTHOR: 'Smith, Peter and Doe, Jane',
		year: 2022,
		month: 11
	}, {
		entrytype: { value: 'ARTICLE', writable: true },
		id: { value: '123456', writable: true}
	})
]

const extries = [
	Object.defineProperties({
		title: 'Foo',
		author: 'Doe, Jane',
		year: 2021,
		month: '@mar'
	}, {
		entrytype: { value: 'article', writable: true },
		id: { value: 'doe2021foo', writable: true}
	}),
	Object.defineProperties({
		title: 'Bar',
		author: 'Smith, Peter and Doe, Jane',
		year: 2022,
		month: '@nov'
	}, {
		entrytype: { value: 'article', writable: true },
		id: { value: '_123456', writable: true}
	})
]


const main = () =>
{
	const thisElem = common.make_test_elem('util')
	
	util.normalize(entries)
	
	if(common.equal_entries(entries, extries) === false)
		return 1
	
	return common.pass(thisElem)
}


main()
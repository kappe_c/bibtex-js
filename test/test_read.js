import read from '../bibtex/read.js'

import common from './common.js'



const legalBib = `## I am a comment that works as a heading
% I am a LaTeX-style comment -- just to be sure
I am a brave raw comment
@comment{I am an explicit comment}

@preamble{"% preamble line 1"}
@preamble{ "% preamble line 2" }

@string{abbr1 = "abbreviation 1"}
@string{abbr2 = {abbreviation 2}}
@string{abbr3 = 3}
@string{abbr4 = "abbreviation 1" # 3}
@string{abbr5 = "abbreviation 1" # abbr4}

@min{myid, foo="bar"}

@article{ doe2021foo
, title = "Foo"
, author = {Doe, Jane}
, year = 2021
}

@article {doe2022bar,
  title ={Bar},
	author= "Smith, Peter and Doe, Jane",
year=2022
}
`


const expectedPreambles = [
	'% preamble line 1',
	'% preamble line 2'
]


const expectedAbbreviations = {
	abbr1: 'abbreviation 1',
	abbr2: 'abbreviation 2',
	abbr3: 3,
	abbr4: ['abbreviation 1', 3],
	abbr5: ['abbreviation 1', '@abbr4']
}


const expectedEntries = [
	Object.defineProperties({
		foo: 'bar'
	}, {
		entrytype: { value: 'min', writable: true },
		id: { value: 'myid', writable: true}
	}),
	Object.defineProperties({
		title: 'Foo',
		author: 'Doe, Jane',
		year: 2021
	}, {
		entrytype: { value: 'article', writable: true },
		id: { value: 'doe2021foo', writable: true}
	}),
	Object.defineProperties({
		title: 'Bar',
		author: 'Smith, Peter and Doe, Jane',
		year: 2022
	}, {
		entrytype: { value: 'article', writable: true },
		id: { value: 'doe2022bar', writable: true}
	})
]


const main = () =>
{
	const thisElem = common.make_test_elem('read')
	
	const introElem = document.createElement('p')
	introElem.textContent = 'Using the following input string'
	
	const inputElem = document.createElement('pre')
	inputElem.textContent = legalBib
	
	thisElem.append(introElem, inputElem)
	
	
	const {entries, abbreviations, preambles} = read(legalBib)
	
// 	console.log('preambles:', preambles)
// 	console.log('abbreviations:', abbreviations)
// 	console.log('entries:', entries)
	
	
	const numPrea = preambles.length
	if(numPrea !== expectedPreambles.length)
		return common.fail(numPrea+' preambles', expectedPreambles.length) || 1
	for(let i = 0; i < numPrea; ++i)
		if(preambles[i] !== expectedPreambles[i])
			return common.fail(preambles[i], expectedPreambles[i]) || 1
	
	if(common.equal_unordered_map(abbreviations, expectedAbbreviations) === false)
		return 1
	
	if(common.equal_entries(entries, expectedEntries) === false)
		return 1
	
	return common.pass(thisElem)
}


main()

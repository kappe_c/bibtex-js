const make_test_elem = name =>
{
	const res = document.body.appendChild(document.createElement('section'))
	
	const heading = document.createElement('h3')
	heading.textContent = name
	res.appendChild(heading)
	
	return res
}


const pass = prnt =>
{
	const outroElem = document.createElement('p')
	outroElem.textContent = 'Test passed'
	prnt.appendChild(outroElem)
	return 0
}


const fail = (have, need) =>
{
	document.body.append(
		'Test failed: have', document.createElement('br'),
		have, document.createElement('br'),
		'need', document.createElement('br'),
		need)
	return false
}


const equal_pair = (common, a, b) =>
{
	const s = typeof a
	const t = typeof b
	if(s !== t)
		return fail(`type ${s} (${a})`, `type ${t} (${b})`)
	if(s !== 'object') // not an Array of concatenations
	{
		if(a === b)
			return true
		if(s === 'string')
		{
			a = a.charAt(0) === '@' ? a.slice(1) : `"${a}"`
			b = b.charAt(0) === '@' ? b.slice(1) : `"${b}"`
		}
		return fail(`${common} = ${a}`, `${common} = ${b}`)
	}
	
	const num = a.length
	if(num !== b.length)
		return fail(`${common} = ${num} concatenated values`, b.length)
	for(let i = 0; i < num; ++i)
		if(equal_pair(i, a[i], b[i]) === false)
			return false
	
	return true
}


const equal_unordered_map = (a, b) =>
{
	const num = Object.keys(a).length
	if(num !== Object.keys(b).length)
		return fail(num+' keys in unordered map', Object.keys(b).length)
	for(const k in a)
		if(equal_pair(k, a[k], b[k]) === false)
			return false
	return true
}


const equal_entry = (a, b) =>
{
	if(typeof a !== 'object')
		return fail('type object', typeof a)
	
	if(a.entrytype !== b.entrytype)
		return fail('entrytype '+a.entrytype, b.entrytype)
	
	if(a.id !== b.id)
		return fail('id '+a.id, b.id)
	
	return equal_unordered_map(a, b)
}


/// The order of the entries but not of the fields is important
const equal_entries = (a, b) =>
{
	const numEntries = a.length
	if(numEntries !== b.length)
		return fail(numEntries+' entries', b.length)
	for(let i = 0; i < numEntries; ++i)
		if(equal_entry(a[i], b[i]) === false)
			return false
	return true
}


export default {
	make_test_elem, pass, fail,
	equal_pair, equal_unordered_map, equal_entries
}
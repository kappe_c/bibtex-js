import conventions from './conventions.js'


/**
 * You may want to call the convenience function id_to_entry instead.
 * @return object
 * 	idToEntry: an object mapping ID to entry (the first with this ID)
 * 	idToCount: an object mapping an ambiguous ID to the number of occurrences
 */
const idToEntry_and_idToExtraEntries = entries =>
{
	const idToEntry = {}
	const idToExtraEntries = {}
	for(const entry of entries)
	{
		const id = entry.id
		if(id in idToEntry)
		{
			if(id in idToExtraEntries)
				idToExtraEntries[id].push(entry)
			else
				idToExtraEntries[id] = [entry]
		}
		else
			idToEntry[id] = entry
	}
	return { idToEntry, idToExtraEntries }
}

/// Get an object containing the entries by ID, show an alert if there are ambiguous ones.
const id_to_entry = entries =>
{
	const { idToEntry, idToExtraEntries } = idToEntry_and_idToExtraEntries(entries)
	const ambiguousIds = Object.keys(idToExtraEntries)
	if(ambiguousIds.length === 0)
		return idToEntry
	
	alert('The following IDs appear multiple times. '+
			'The extra ones have been added an "__2" etc.\n'+
			ambiguousIds.map(a => `${a}: ${idToExtraEntries[a].length+1}`).join('\n'))
	
	for(const id of ambiguousIds)
		idToExtraEntries[id].forEach((v, k) =>
		{
			const jd = id+'__'+(k+2)
			// unlikely:
			console.assert(idToEntry[jd] === undefined, `Error: fallback ID ${jd} does already exist`)
			v.id = jd
			idToEntry[jd] = v
		})
	return idToEntry
}

/// @return true if the initial character is lowercase
const initial_is_lower = a => a.charAt(0) === a.charAt(0).toLowerCase()

/**
 * If no comma and no blank is in a name string, it is assumed to be the last name.
 * @param name is a string in one of the formats allowed by BibTeX (no abbreviation)
 * @return an object with the properties *first*, *von*, *last* and *jr*
 */
const name_object = name =>
{
	const res = {}
	// BibTeX recognizes three possible formats; they are distinguished by the number of commas.
	const parts = name.split(', ')
	switch(parts.length)
	{
	case 3: // von Last, Jr, First
		res.jr = parts[1]
		// Except for the *jr* part between the commas, this is the same as the next case.
		// => no break
		parts.splice (1, 1) // This is inefficient but there is rarely a *jr* part anyway
	case 2: // von Last, First
	{
		res.first = parts[1]
		const vonLastTokens = parts[0].split(' ')
		
		let vonEnd
		const n = vonLastTokens.length-1
		for(let i = 0; i < n; ++i)
			if(initial_is_lower(vonLastTokens[i]))
				vonEnd = i+1
		
		if(vonEnd === undefined)
			res.last = parts[0]
		else {
			res.von  = vonLastTokens.slice(0, vonEnd).join(' ')
			res.last = vonLastTokens.slice(vonEnd).join(' ')
		}
		break
	}
	case 1: // First von Last
	{
		const tokens = parts[0].split(' ')
		const n = tokens.length-1
		
		let vonBeg, vonEnd
		for(let i = 1; i < n; ++i)
		{
			if(initial_is_lower(tokens[i]))
			{
				if(vonBeg === undefined)
					vonBeg = i
				vonEnd = i+1
			}
		}
		
		if(vonBeg !== undefined)
		{
			res.first = tokens.slice(0, vonBeg).join(' ')
			res.von   = tokens.slice(vonBeg, vonEnd).join(' ')
			res.last  = tokens.slice(vonEnd).join(' ')
		}
		else // The last name is assumed to consist only of one token
		{
			res.first = tokens.slice(0, n).join(' ') // may be the empty string
			res.last = tokens[n]
		}
		break
	}
	default:
		alert('Ill-formatted name: '+name)
		res.last = authorStr
	}
	return res
}

/**
 * Put a BibTeX name in the order "von Last, Jr, First" if more than one of these four parts is present.
 * Abbreviations are left untouched.
 */
const normalize_name = name =>
{
	if(name.charAt(0) === '@')
		return name
	const o = name_object(name)
// 	let res = o.von === undefined ? o.last+', ' : o.von+' '+o.last+', '
	let res = o.von === undefined ? o.last : o.von+' '+o.last
	if(o.jr !== undefined)
		res += ', '+o.jr
	return o.first === '' ? res : res+', '+o.first
}


const authorSep  = conventions.fieldToSeparator.author
const authorJoin = conventions.fieldToJoin.author

/// Normalize an author-like value (e.g. also editor), see normalize_name.
const normalize_author_like = (value, sep=authorSep, join=authorJoin) =>
{
	if(typeof value === 'string')
		return value.split(sep).map(normalize_name).join(join)
	// We assume the value is not a number
	const res = []
	// Concatenations may be arbitrary, so we blindly join first, 
	// even though they may already nicely separate the authors.
	value.join('').split(sep).forEach((v, k) => res[k*2] = normalize_name(v))
	const n = res.length
	for(let k = 1; k < n; k+=2)
		res[k] = join
	return res
}

/**
 * Make all entry types and fields lowercase; 
 * prepend an underscore to IDs that start with a digit; 
 * replace the *month* value with the appropriate abbreviation; 
 * convert strings that are actually numbers.
 */
const normalize = entries =>
{
	const abbreviations = conventions.abbreviations
	const monthNumToMonthAbbr = conventions.monthNumToMonthAbbr
	const numericFields = conventions.numericFields
	for(const entry of entries)
	{
		entry.entrytype = entry.entrytype.toLowerCase()
		
		// bibtex-js allows IDs that start with a digit, 
		// for maximum compatibility we prepend an underscore.
		if(/^\d/.test(entry.id) === true)
		{
			console.log('Changing ID', entry.id, 'to _' + entry.id)
			entry.id = '_' + entry.id
		}
			
		// Find the non-lowercase fields
		const oldfToNewf = {}
		for(const field in entry)
		{
			const lower = field.toLowerCase()
			if(field !== lower)
				oldfToNewf[field] = lower
		}
		// Normalize the non-lowercase fields
		for(const oldf in oldfToNewf)
		{
			entry[oldfToNewf[oldf]] = entry[oldf]
			delete entry[oldf]
		}
		
		// Try to normalize the month
		const month = entry.month
		if(month !== undefined)
		{
			if(month in monthNumToMonthAbbr) // e.g. month===9 or month==='9'
				entry.month = monthNumToMonthAbbr[month]
			// Sometimes a string literal is used instead of an abbreviation
			else if(typeof month === 'string')
			{
				const short = month.slice(0, 3).toLowerCase()
				if(short in abbreviations)
					entry.month = '@'+short
			}
		}
		
		// Check for ill-formatted numbers
		for(const field in entry) if(field in numericFields)
		{
			const v = entry[field]
			if(typeof v === 'string' && v.charAt(0) !== '@')
			{
				// Only integers not real numbers; no leading zeros allowed but whitespace;
				// base 10 unless prefixed with "0x" or "0X" (then 16).
				const a = parseInt(v)
				if(isNaN(a) === false)
					entry[field] = a
			}
		}
		
		// Check for ill-formatted page ranges
		const pages = entry.pages
		if(typeof pages === 'string')
		{
			const res = pages.match(/^(\d+)-(\d+)$/)
			if(res !== null)
				entry.pages = `${res[1]}--${res[2]}`
		}
		
		if('author' in entry)
			entry.author = normalize_author_like(entry.author)
// 		if('editor' in entry) // We find this too rare to handle it in general
// 			entry.editor = normalize_author_like(entry.editor)
	}
	return entries
}

/**
 * Sort the entries w.r.t. the fieldInfos, i.e. first sort by fieldInfos[0][0];
 * the second info element (1 or -1) determines ascending or descending order;
 * the third info element may be mapping function for the field values.
 * Return the input array.
 */
const sort_entries = (entries, fieldInfos=conventions.fieldInfosForSorting) =>
{
	const compare_entry = (ea, eb) =>
	{
		// result < 0 makes a come before b
		for(const fieldInfo of fieldInfos)
		{
			const f = fieldInfo[0]
			let va = ea[f]
			let vb = eb[f]
			if(va == vb) // not === on purpose
				continue
			if(va === undefined)
				return 1*fieldInfo[1] // undefined at the end
			if(vb === undefined)
				return -1*fieldInfo[1]
			const map = fieldInfo[2]
			if(map !== undefined)
			{
// 				console.log(va, vb)
				va = map(va)
				vb = map(vb)
// 				console.log(va, vb)
// 				console.log(va, '<', vb, ':', va < vb)
			}
			return (va < vb ? -1 : 1)*fieldInfo[1]
		}
		return 0
	}
	return entries.sort(compare_entry)
}

/// Sort the fields in the entries w.r.t. groupedAndSortedFields, return the input array
const sort_fields = (entries, fieldToRank=conventions.fieldToRank) =>
{
	const compare_field = (fa, fb) =>
	{
		const ra = fieldToRank[fa]
		const rb = fieldToRank[fb]
		if(ra === rb)
			return 0
		if(ra === undefined)
			return 1
		if(rb === undefined)
			return -1
		return ra-rb
	}
	
	const numEntries = entries.length
	for(let i = 0; i < numEntries; ++i)
	{
		// Create a new entry with the fields inserted in the desired order
		const olde = entries[i]
		const newe = Object.defineProperties({},
		{
			entrytype: { value: olde.entrytype, writable: true },
			id: { value: olde.id, writable: true }
		})
		for(const field of Object.keys(olde).sort(compare_field))
			newe[field] = olde[field]
		entries[i] = newe
	}
	return entries
}

/// Equivalent to calling sort_entries and sort_fields
const sort_entries_and_fields = (entries, fieldInfos) =>
	sort_fields(sort_entries(entries, fieldInfos))


export default {
	id_to_entry, idToEntry_and_idToExtraEntries, 
	normalize, sort_entries_and_fields, sort_entries, sort_fields,
	normalize_author_like, normalize_name, name_object
}
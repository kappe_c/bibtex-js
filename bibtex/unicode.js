/**
 * @author Christopher Kappe <christopher.kappe+pub@gmail.com>
 * @established 2019-08-23
 */

const dict = { // from recognized command to translated string
	'ss':'\u00DF', // sharp s (esszett)
	'o': '\u00F8', // slashed o (o with stroke)
	'O': '\u00D8', // slashed O (O with stroke)
	'i': '\u0131', // i without dot
	'j': '\u0237', // j without dot
	'l': '\u019A', // barred l (l with stroke)
	'L': '\u0197', // barred L (L with stroke)
	'rq':'\u2019', // right single quotation mark (This is the preferred character to use for apostrophe.)
}

const symbols = { // directly followed by argument
	'`': '\u0300', // grave
	"'": '\u0301', // acute
	'^': '\u0302', // circumflex
	'"': '\u0308', // umlaut, trema or dieresis
	'~': '\u0303', // tilde
	'=': '\u0304', // macron (bar above letter)
	'.': '\u0307', // dot above letter
}

const letters = { // need argument in braces
	'H': '\u030B', // long Hungarian umlaut (double acute)
	'c': '\u0327', // cedilla
	'k': '\u0328', // ogonek
	'b': '\u0331', // macron below (bar below letter)
	'd': '\u0323', // dot below letter
	'r': '\u030A', // ring above letter
	'u': '\u0306', // breve (small "u" above letter)
	'v': '\u030C', // caron (small "v" above letter)
}

const args = [ // letters that may be modified
	'a', 'A', 'e', 'E', '\u0131', 'I', 'o', 'O', 'u', 'U', 'c', 'C', 'r', 'R', 's', 'S']

for(const arg of args)
{
	for(const symbol in symbols)
	  dict[symbol + arg] = arg + symbols[symbol]
	for(const letter in letters)
	  dict[letter+'{'+arg+'}'] = arg + letters[letter]
}

// To use the dictionary entries directly in a RegExp, we add the backslash; and surround them
// with braces because that is how they typically appear in bib files.
for(const key in dict)
{
	dict['{\\'+key+'}'] = dict[key]
	delete dict[key]
}

// We do not intend to scan recursively, so we add the following common commands.
// The outer command is not surrounded by braces in these cases.
dict["\\'{\\i}"] = '\u00ED' // i with acute instead of dot
dict['\\"{\\i}'] = '\u00EF' // i with diaeresis instead of dot

// Quotation marks (no backslash or braces)
dict['``'] = '\u201C' // English left double
dict["''"] = '\u201D' // English right double
dict['"`'] = '\u201E' // German left double
dict[`"'`] = '\u201C' // German right double (same as English left double)
// It is important that these come after the others
dict['`'] = '\u2018' // English left single
dict["'"] = '\u2019' // English right single (also apostrophe in all languages)

//------------------------------------------------------------------------------------------------//

// characters that need to be escaped in a RegExp
const specialCharArr = [ '\\',
	'^', '$', '|', '.', '=', ':', '!', '-',
	'?', '*', '+',
	'(', ')', '[', ']', '{', '}', '<', '>',
]

// By definition, each of the characters needs to be escaped when used to construct a RegExp.
const specialCharRegex = new RegExp('['+specialCharArr.map(x => '\\'+x).join('')+']', 'g')

// Replace any special character with itself preceded by as backslash.
const escape = str => str.replace(specialCharRegex, '\\$&')

// RegExp that matches any dictionary entry.
const dictRegex = new RegExp(Object.keys(dict).map(x => escape(x)).join('|'), 'g')

// Replace any dictionary key that appears in the string with its associated value.
const translate_str = str =>
{
// 	console.log(`>>>${str}<<<`)
	return str.replace(dictRegex, key =>
	{
// 		console.log(key, '->', dict[key])
		return dict[key]
	})
}

// Fields (lowercase) in which translatable characters are expected
const fieldsWithSpecialChars = ['title', 'author', 'editor', 'publisher', 'journal']

/**
 * Apply *translate* to all the fields from *fieldsWithSpecialChars* for all the entries; 
 * return the input array processed inplace.
 */
const translate = entries =>
{
	const is_str = a => (typeof a === 'string' && a.charAt(0) !== '@')
	const is_arr = Array.isArray
	for(const entry of entries)
		for(const field of fieldsWithSpecialChars) if(field in entry)
		{
			const a = entry[field]
// 			console.log(`>>>${a}<<<`)
			if(is_str(a) === true) entry[field] = translate_str(a)
			else if(is_arr(a) === true)
				a.forEach((v, k) => { if(is_str(v) === true) a[k] = translate_str(v) })
		}
	return entries
}

//------------------------------------------------------------------------------------------------//

/**
 * Translate LaTeX commands that represent a single unicode character to JavaScript strings.
 * https://en.wikibooks.org/wiki/LaTeX/Special_Characters#Escaped_codes
 * https://en.wikipedia.org/wiki/Combining_character#Unicode_ranges
 */
export default {
	dict, fieldsWithSpecialChars, 
	translate
}
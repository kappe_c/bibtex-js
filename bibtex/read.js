// We use the following terms for the components of a bib file.
// entry (in other contexts called object, map, dictionary),
// entry type (in other contexts called class),
// entry ID (in other contexts called name), 
// field (in other contexts called attribute, property, key),
// value (right-hand side of an equals sign, either string, number, abbreviation or concatenation),
// abbreviation (in other contexts called variable, defined in an @string block)

// Define regular expressions used in the read function.
// Notice that lastIndex (read/write) specifies the index at which to start the next match.
// If the g flag is set, it is either because the exec method is used in a loop over one string,
// or because another RegExp needs the lastIndex of this one (they operate on the same string).

// We expect entry types, fields and therefore abbreviations to consist of a non-empty sequence of
// alphanumeric characters from the basic Latin alphabet, including the underscore, but
// the first character must not be a digit.
// This is stricter than the bib format allows but good practice as it enables compatibility with
// other file formats.
// An entry ID may start with a digit because sometimes a numeric identifier is used but this is
// discouraged.


// @ as the first non-whitespace character in a line or the string,
// the m flag makes ^ match not only the global begin also the beginning of a line.
// Capture at least one non-digit character until "{"
const regexType = /^\s*@([A-Za-z_]\w*)\s*\{\s*/gm

// For the following regular expressions we assume that all leading whitespace has already
// been consumed

// Capture at least one \w until "," or "}" (allowing empty entries)
const regexId = /(\w+)\s*([,}])\s*/y
const regexIdRelaxed = /(.+)\s*([,}])\s*/y

// Capture at least one one non-digit character until "="
const regexField = /([A-Za-z_]\w*)\s*=\s*/y

// Capture at least one of
// - anything but a double-quote or a backslash or
// - a backslash followed by anything (in particular the sequence backslash-double-quote,
//   i.e. an escaped double-quote)
// until "," or "}" or "#" which is the second capture
const regexQuoted = /"((?:[^"\\]|\\.)+)"\s*([,}#])\s*/y

// Capture at least one \w until "," or "}" or "#" which is the second capture
const regexNumOrAbbr = /(\w+)\s*([,}#])\s*/y

// To be used after till_imbalanced_closing_brace
const regexDelim = /\s*([,}#])\s*/y
// Balanced delimiters are not supported in JavaScript regular expressions


/**
 * This is not intended to read blocks (because it consumes the closing brace which may be necessary to recognize the end of the last value, and it is relatively inefficient) but only for string literals enclosed by braces; any brace escaped by a backslash is ignored.
 * @param {string} iStr String to read through.
 * @param {number} beg Index of the first character to read. It is expected that this points to the first character after an opening brace.
 * @return {string} String from the first character through (excluding) the next imbalanced closing brace.
 * @throw {SyntaxError} In case there is no more imbalanced closing brace.
 */
const till_imbalanced_closing_brace = (iStr, beg) =>
{
	const arr = [] // collect the result
	let numImbalancedOpenings = 0 // not counting the initial (outermost) one
	const end = iStr.length
	for(let i = beg; i < end; ++i)
	{
		const x = iStr.charAt(i)
		if(x === '}')
		{
			if(numImbalancedOpenings === 0)
				return arr.join('') // also works for empty array
			--numImbalancedOpenings
		}
		else if(x === '{')
		{
			++numImbalancedOpenings
		}
		else if(x === '\\')
		{
			// There is an escaped character, possibly a brace, add it blindly.
			// E.g. title = {This Text about the Symbol \{ is Complicated}
			arr.push(x, iStr.charAt(++i))
			continue
		}
		arr.push(x)
	}
	throw new SyntaxError('no closing brace here:\n'+iStr.slice(beg, beg+80))
}


/**
 * Read field-value pairs from `iStr` starting at `beg` into `oObj`.
 * @return {number} Index into `iStr` pointing behind the closing brace of the block and possible whitespace
 */
const read_field_value_pairs = (iStr, beg, oObj) =>
{
	let delim
	
	const read_simple_value = () =>
	{
// 		console.debug('iStr.charAt(beg):', iStr.charAt(beg))
		
		if(iStr.charAt(beg) === '"')
		{
			regexQuoted.lastIndex = beg
			const resQ = regexQuoted.exec(iStr)
			if(resQ !== null)
			{
				beg = regexQuoted.lastIndex
				delim = resQ[2]
				return resQ[1]
			}
		}
		
		if(iStr.charAt(beg) === '{')
		{
			const braced = till_imbalanced_closing_brace(iStr, beg+1)
			regexDelim.lastIndex = beg+braced.length+2
			const resDelim = regexDelim.exec(iStr)
			if(resDelim === null)
				throw new SyntaxError('no comma, closing brace or hash sign after braced string:\n'+iStr.slice(beg, beg+80))
			beg = regexDelim.lastIndex
			delim = resDelim[1]
			return braced
		}
		
		regexNumOrAbbr.lastIndex = beg
		const resNorA = regexNumOrAbbr.exec(iStr)
		if(resNorA !== null)
		{
			beg = regexNumOrAbbr.lastIndex
			delim = resNorA[2]
			const asNum = Number(resNorA[1])
			if(asNum !== asNum) // only true if asNum is NaN
				return '@'+resNorA[1]
			else
				return asNum
		}
		
		throw new SyntaxError('no value here:\n'+iStr.slice(beg, beg+80))
	}
	
	while(delim !== '}')
	{
		regexField.lastIndex = beg
		const resF = regexField.exec(iStr)
		if(resF === null)
			throw new SyntaxError('no field here:\n'+iStr.slice(beg, beg+80))
		
		beg = regexField.lastIndex
		let val = read_simple_value()
		
		if(delim === '#')
		{
			val = [val]
			do val.push(read_simple_value())
			while(delim === '#');
		}
		
		oObj[resF[1]] = val
	}
	return beg
}


/**
 * Parse the content of a bib file. All comments are ignored. 
 * A value (rhs of an equals sign) is either a string, a number, a string that starts with an "@" (abbreviation) or an array (concatenation of any of the above).
 * @param {string} iStr Text string read from a bib file.
 * @return {object}
 *     entries {Array} Objects with properties according to an entry's fields, plus one *id* property for the entry identifier before the field-value pairs, and an *entrytype* property for the type name after the `@`. These two properties are configured to not be enumerable so that one can iterate over the fields of an entry with `for(const field in entry)`.
 *     abbreviations {object} Abbreviations defined in `@string` blocks
 *     preambles {Array} Strings of LaTexX code defined in `@preamble` blocks
 * @throw {SyntaxError} In case the file cannot be parsed.
 */
const read = iStr =>
{
	if(typeof iStr !== 'string')
	{
		console.error('Input is not a string')
		return null
	}
	const entries = []
	const abbreviations = {}
	const preambles = []
	
	// Iterate over blocks
	regexType.lastIndex = 0 // in case the function has been called before already
	for(let resType = regexType.exec(iStr); resType !== null; resType = regexType.exec(iStr))
	{
		const idx = regexType.lastIndex
		const type = resType[1]
		const typeLowerCase = type.toLowerCase()
		
		if(typeLowerCase === 'comment')
			continue
		
		if(typeLowerCase === 'preamble')
		{
			regexQuoted.lastIndex = idx
			const resQ = regexQuoted.exec(iStr)
			if(resQ === null)
				throw new SyntaxError('no quoted string in `@preamble`:\n'+iStr.slice(idx, idx+80))
			preambles.push(resQ[1])
			regexType.lastIndex = regexQuoted.lastIndex
			continue
		}
		
		if(typeLowerCase === 'string')
		{
			regexType.lastIndex = read_field_value_pairs(iStr, idx, abbreviations)
			continue
		}
		
		regexId.lastIndex = idx
		let resId = regexId.exec(iStr)
		if(resId === null) {
			// throw new SyntaxError('No valid ID here:\n'+iStr.slice(idx, idx+80))
			console.warn('No (strictly) valid ID here:\n'+iStr.slice(idx, idx+80))
			regexIdRelaxed.lastIndex = idx
			resId = regexIdRelaxed.exec(iStr)
			regexId.lastIndex = regexIdRelaxed.lastIndex
		}
		
		// We define the "pseudo fields" `entrytype` and `id` so that they are not enumerable, i.e.
		// they do not appear in `Object.keys(entry)` or `for(const field in entry)`.
		const entry = Object.defineProperties({},
		{
			entrytype: { value: type, writable: true },
			id: { value: resId[1], writable: true }
		})
		entries.push(entry)
		if(resId[2] === '}')
			continue
		
		regexType.lastIndex = read_field_value_pairs(iStr, regexId.lastIndex, entry)
	}
	
	return {entries, abbreviations, preambles}
}



export default read
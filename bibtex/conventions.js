


/// Known abbreviations
const abbreviations = {
	jan: 'January', feb: 'February', mar: 'March',
	apr: 'April', may: 'May', jun: 'June',
	jul: 'July', aug: 'Aug', sep: 'September',
	oct: 'October', nov: 'November', dec: 'December'
}

/// Array mapping the numbers 1-12 to the three-letter month abbreviations
const monthNumToMonthAbbr = [
	, '@jan', '@feb', '@mar'
	, '@apr', '@may', '@jun'
	, '@jul', '@aug', '@sep'
	, '@oct', '@nov', '@dec'
]
for(let i=1; i<10; ++i)
	monthNumToMonthAbbr[`0${i}`] = monthNumToMonthAbbr[i]

/// Object mapping the month abbreviations to numbers 1-12, e.g. for sorting
const monthAbbrToMonthNum = (() => // result of directly calling an anonymous function
{
	const res = {}
	monthNumToMonthAbbr.forEach((v, k) => res[v] = k)
// 	console.log(res)
// 	console.log(typeof res['@jan'])
// 	console.log(res['@sep'] < res['@oct'], '<- should be true')
	return res
})()

/// Fields that may contain numeric values
const numericFields =
{
	year: true, day: true, // recall that month should be an abbreviation or a concatenation
	volume: true, number: true, pagetotal: true,
	edition: true, chapter: true,
	mystar: true,
}

/// Fields in the order in which they are used for sorting entries by default 
/// (with 1/-1 stating ascending or descending order)
const fieldInfosForSorting = [
	['year', 1],
	['month', 1, a => monthAbbrToMonthNum[a]],
	['day', 1],
	['author', 1],
	['title', 1],
]

/**
 * Object of arrays of frequently-used fields ordered by their serialization rank 
 * (the programmer's personal preference).
 * Use `for(const groupName of Object.keys(groupedAndSortedFields))` to be sure the order 
 * is maintained (since ES5).
 */
const groupedAndSortedFields =
{
	essential: ['title', 'author', 'booktitle'],
	date: ['date', 'year', 'month', 'day', 'origdate'],
	conference: ['organization', 'eventtitle', 'eventdate', 'venue'],
	journal: ['journal', 'series', 'volume', 'number', 'pages', 'publisher', 'address'],
	rare: ['editor', 'edition', 'chapter', 'institution', 'school', 'location'],
	id: ['doi', 'isbn', 'issn', 'acmid', 'eprint', 'url'],
	exotic: ['type', 'key', 'crossref'],
	extra: ['pagetotal', 'note', 'keywords', 'abstract'],
	personal: ['localfile', 'myprojects', 'mystar', 'mytags'],
}

/// Sorted fields (w.r.t. groupedAndSortedFields but without the grouping)
const rankToField = (() => {
	const res = []
	for(const groupName of Object.keys(groupedAndSortedFields))
		res.push(...groupedAndSortedFields[groupName])
	return res
})()

/// Object mapping several frequent fields to their rank (see above)
const fieldToRank = (() => {
	const res = {}
	rankToField.forEach((field, rank) => res[field] = rank)
	return res
})()

/// For fields that may contain multiple values, store the regular expression for separation
const fieldToSeparator =
{
	author: /\s+and\s+/,
	editor: /\s+and\s+/,
	keywords: /\s*;\s*/,
	mytags: ',',
	myprojects: ',',
}

/// For fields that may contain multiple values, store the string for joining
const fieldToJoin =
{
	author: ' and ',
	editor: ' and ',
	keywords: ';',
	mytags: ',',
	myprojects: ',',
}


export default {
	abbreviations, monthNumToMonthAbbr, monthAbbrToMonthNum, numericFields, 
	fieldInfosForSorting, groupedAndSortedFields, rankToField, fieldToRank, 
	fieldToSeparator, fieldToJoin
}



/**
 * @return a (nicely formatted) string that can be written to a bib file
 * @param entries must be iterable
 * @param abbreviations may be undefined, null or an (empty) object
 * @param preambles may be undefined, null or iterable with a length property
 */
const write = (entries, abbreviations, preambles) =>
{
	const oArr = []
	
	if(Symbol.iterator in Object(preambles) && preambles.length > 0)
	{
		for(const preamble of preambles)
			oArr.push(`@preamble{${preamble}}`)
		oArr.push('')
	}
	
	if(typeof abbreviations === 'object' && abbreviations !== null)
	{
		const abbrs = Object.keys(abbreviations)
		if(abbrs.length > 0)
		{
			for(const a of abbrs)
				oArr.push(`@string{${a} = ${abbreviations[a]}}`)
			oArr.push('')
		}
	}
	
	const valToOut = a => // atomic value (not concatenation) to string
	{
		if(typeof a === 'number')
			return a
		if(a.charAt(0) === '@')
			return a.slice(1)
		return `"${a}"`
	}
	
	for(const entry of entries)
	{
		oArr.push(`@${entry.entrytype}{ ${entry.id}`)
		for(const field of Object.keys(entry))
		{
			const v = entry[field]
			const t = typeof v
			oArr.push(`, ${field} = ${t === 'object' ? v.map(valToOut).join('#') : valToOut(v)}`)
		}
		oArr.push('}\n')
	}
	// Avoid ending with an empty line
	oArr.pop()
	oArr.push('}')
	
	return oArr.join('\n')
}


export default write